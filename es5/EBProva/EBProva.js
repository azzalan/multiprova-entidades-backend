'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EBProva = undefined;

var _multiprovaEntidades = require('multiprova-entidades');

var _propsEBProva = require('./propsEBProva');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EBProva = exports.EBProva = function (_Prova) {
  _inherits(EBProva, _Prova);

  function EBProva(data, operationType) {
    _classCallCheck(this, EBProva);

    var _this = _possibleConstructorReturn(this, (EBProva.__proto__ || Object.getPrototypeOf(EBProva)).call(this, null, operationType));

    _this.construirBack(_propsEBProva.propsEBProva, data);
    return _this;
  }

  return EBProva;
}(_multiprovaEntidades.Prova);