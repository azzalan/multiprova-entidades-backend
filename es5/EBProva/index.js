'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _EBProva = require('./EBProva');

Object.defineProperty(exports, 'EBProva', {
  enumerable: true,
  get: function get() {
    return _EBProva.EBProva;
  }
});