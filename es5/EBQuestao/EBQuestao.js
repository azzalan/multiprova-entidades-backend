'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EBQuestao = undefined;

var _multiprovaEntidades = require('multiprova-entidades');

var _propsEBQuestao = require('./propsEBQuestao');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EBQuestao = exports.EBQuestao = function (_Questao) {
  _inherits(EBQuestao, _Questao);

  function EBQuestao(data, operationType) {
    _classCallCheck(this, EBQuestao);

    var _this = _possibleConstructorReturn(this, (EBQuestao.__proto__ || Object.getPrototypeOf(EBQuestao)).call(this, null, operationType));

    _this.construirBack(_propsEBQuestao.propsEBQuestao, data);
    return _this;
  }

  return EBQuestao;
}(_multiprovaEntidades.Questao);