'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _EBQuestao = require('./EBQuestao');

Object.defineProperty(exports, 'EBQuestao', {
  enumerable: true,
  get: function get() {
    return _EBQuestao.EBQuestao;
  }
});