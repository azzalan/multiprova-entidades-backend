"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var propsEBQuestao = exports.propsEBQuestao = {
  dataCadastro: {
    auto: {
      create: function create() {
        return new Date();
      }
    }
  },
  dataUltimaAlteracao: {
    auto: {
      create: function create() {
        return new Date();
      },
      update: function update() {
        return new Date();
      }
    }
  },
  criadoPor: {
    auto: {
      create: function create(data) {
        if (data.accessToken) return data.accessToken.userId;
      }
    }
  }
  // userId: {
  //   auto: {
  //     create: data => {
  //       if (data.accessToken) return data.accessToken.userId
  //     },
  //   },
  // },
};