"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var propsEBUsuario = exports.propsEBUsuario = {
  dataCadastro: {
    auto: {
      create: function create() {
        return new Date();
      }
    }
  },
  dataUltimaAlteracao: {
    auto: {
      create: function create() {
        return new Date();
      },
      update: function update() {
        return new Date();
      }
    }
  },
  criadoPor: {
    auto: {
      create: function create(data) {
        if (data.accessToken) return data.accessToken.userId;
      }
    }
  }
};