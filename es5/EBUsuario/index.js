'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _EBUsuario = require('./EBUsuario');

Object.defineProperty(exports, 'EBUsuario', {
  enumerable: true,
  get: function get() {
    return _EBUsuario.EBUsuario;
  }
});