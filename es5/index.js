'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _EBUsuario = require('./EBUsuario');

Object.defineProperty(exports, 'EBUsuario', {
  enumerable: true,
  get: function get() {
    return _EBUsuario.EBUsuario;
  }
});

var _EBQuestao = require('./EBQuestao');

Object.defineProperty(exports, 'EBQuestao', {
  enumerable: true,
  get: function get() {
    return _EBQuestao.EBQuestao;
  }
});

var _EBProva = require('./EBProva');

Object.defineProperty(exports, 'EBProva', {
  enumerable: true,
  get: function get() {
    return _EBProva.EBProva;
  }
});