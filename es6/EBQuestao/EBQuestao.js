import { Questao } from 'multiprova-entidades'
import { propsEBQuestao } from './propsEBQuestao'

export class EBQuestao extends Questao {
  constructor(data, operationType) {
    super(null, operationType)
    this.construirBack(propsEBQuestao, data)
  }
}
