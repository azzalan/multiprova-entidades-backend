import { Usuario } from 'multiprova-entidades'
import { propsEBUsuario } from './propsEBUsuario'

export class EBUsuario extends Usuario {
  constructor(data, operationType) {
    super(null, operationType)
    this.construirBack(propsEBUsuario, data)
  }
}
