import { EBUsuario } from './EBUsuario'

describe('Usuario', () => {
  it('cria', () => {
    const data = {
      nome: 'ODocente SobrenomeDocente',
      instituicao: 'UFRN',
      permissoes: [2],
      username: 'docente',
      email: 'docente@ufrn.br',
      password: '12345678',
    }
    const usuario = new EBUsuario(data, 'create')
    expect(usuario.operationType).toBe('create')
    expect(usuario.isValid()).toBeTruthy()
    expect(usuario.data.nome).toBe(data.nome)
    expect(usuario.data.instituicao).toBe(data.instituicao)
    expect(usuario.data.permissoes).toBe(data.permissoes)
    expect(usuario.data.username).toBe(data.username)
    expect(usuario.data.email).toBe(data.email)
    expect(usuario.data.dataCadastro).toBeTruthy()
  })
})
