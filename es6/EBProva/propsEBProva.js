export const propsEBProva = {
  dataCadastro: {
    auto: {
      create: () => new Date(),
    },
  },
  dataUltimaAlteracao: {
    auto: {
      create: () => new Date(),
      update: () => new Date(),
    },
  },
  criadoPor: {
    auto: {
      create: data => {
        if (data.accessToken) return data.accessToken.userId
      },
    },
  },
  // userId: {
  //   auto: {
  //     create: data => {
  //       if (data.accessToken) return data.accessToken.userId
  //     },
  //   },
  // },
}
