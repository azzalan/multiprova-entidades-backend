import { Prova } from 'multiprova-entidades'
import { propsEBProva } from './propsEBProva'

export class EBProva extends Prova {
  constructor(data, operationType) {
    super(null, operationType)
    this.construirBack(propsEBProva, data)
  }
}
